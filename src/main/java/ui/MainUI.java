package ui;

import com.formdev.flatlaf.FlatLightLaf;
import com.formdev.flatlaf.IntelliJTheme;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import tools.ENVARS_WORKER;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.util.Map;
import java.util.Objects;

public class MainUI
{

    private JPanel mainPane;
    private JPanel left_panel;
    private JList<String> main_list;
    private JPanel logo_pane;
    private JLabel logo_label;
    private JScrollPane list_scroll_pane;
    private JSplitPane right_panel;
    private JTextArea ta_plain_text;
    private JScrollPane plaint_text_pane;
    private JButton btn_refresh;
    private JButton btn_add;
    private JButton btn_remove;
    private JCheckBox cbx_filter;
    private JTree tree;
    private JButton btn_update;
    private JList<String> list1;

    private DefaultListModel<String> listModel;
    private Map<String, String> currentVars;
    private boolean showOnlyCustom;

    public MainUI()
    {
        btn_refresh.addActionListener(e -> fillVarList());
        cbx_filter.addItemListener(e -> {
            showOnlyCustom = cbx_filter.isSelected();
            fillVarList();
        });

        setButtonTransparent(btn_add);
        setButtonTransparent(btn_refresh);
        setButtonTransparent(btn_remove);
        setButtonTransparent(btn_update);

        btn_update.addActionListener(e ->
        {
            String selectedVar = main_list.getSelectedValue();
            String currText = ta_plain_text.getText();
            if(selectedVar == null || currText == null || currText.trim().isEmpty()) {
                JOptionPane.showMessageDialog(null,
                        "Select an env var",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
            boolean success = ENVARS_WORKER.setEnvVar(selectedVar,currText);
            JOptionPane.showMessageDialog(null,
                    success?"Success":"Failed",
                    "Information",
                    success?JOptionPane.INFORMATION_MESSAGE:JOptionPane.ERROR_MESSAGE);

        });

        btn_remove.addActionListener(e ->
        {
            String selectedVar = main_list.getSelectedValue();
            if(selectedVar == null) return;
            boolean success = ENVARS_WORKER.dropEnVar(selectedVar);
            JOptionPane.showMessageDialog(null,
                    success?"Success, you may need to restart PC (or terminals) to see changes":"Failed",
                    "Information",
                    success?JOptionPane.INFORMATION_MESSAGE:JOptionPane.ERROR_MESSAGE);
        });

        btn_add.addActionListener(e ->
        {
            String name = JOptionPane.showInputDialog(null,"Variable name");
            if(name != null && !name.trim().isEmpty())
            {
                boolean success = ENVARS_WORKER.setEnvVar(name, "{}");
                JOptionPane.showMessageDialog(null,
                    success?"Success":"Failed","Information",
                    success?JOptionPane.INFORMATION_MESSAGE:JOptionPane.ERROR_MESSAGE);
            }
        });

        initData();
    }

    private void initData()
    {
        showOnlyCustom = cbx_filter.isSelected();
        listModel = new DefaultListModel<>();
        main_list.setModel(listModel);
        main_list.addListSelectionListener(e -> {
            if(e.getValueIsAdjusting()) return;
            String selected = main_list.getSelectedValue();
            if(selected == null) return;
            updateSelected(selected);
        });
        fillVarList();
    }

    private void fillVarList()
    {
        System.out.println("Updating env vars...");
        listModel.clear();
        main_list.updateUI();
        cleanTree(tree);
        currentVars = ENVARS_WORKER.getEnvVars();
        currentVars.forEach((name, value) -> {
            if(showOnlyCustom && !value.startsWith("{")) return;
            listModel.addElement(name);
        });
    }

    private void updateSelected(String selected)
    {
        String value = currentVars.get(selected);
        ta_plain_text.setText(value);
        cleanTree(tree);
        fillTree(value, selected);
    }

    private void fillTree(String jsonString, String varName)
    {
        JsonObject jsonObject = ENVARS_WORKER.getJsonFromString(jsonString);
        if(jsonObject == null) return;
        DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(varName);
        recurseFillTree(jsonObject,root);
        model.setRoot(root);
    }

    private void recurseFillTree(JsonObject rootJson, DefaultMutableTreeNode root)
    {
        for(Map.Entry<String, JsonElement> entry : rootJson.entrySet())
        {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(entry.getKey());
            if(entry.getValue().isJsonObject())
                recurseFillTree(entry.getValue().getAsJsonObject(),node);
            else
                node.add(new DefaultMutableTreeNode(entry.getValue()));
            root.add(node);
        }
    }

    private void setButtonTransparent(JButton button)
    {
        button.setOpaque(false);
        button.setContentAreaFilled(false);
        button.setBorderPainted(false);
    }

    private void cleanTree(JTree jTree)
    {
        DefaultTreeModel model = (DefaultTreeModel)jTree.getModel();
        DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
        if(root == null) return;
        root.removeAllChildren();
        model.reload();
        model.setRoot(null);
    }

    public static void main(String[] args)
    {
        UIManager.put("ProgressBar.cycleTime", 1000);
        UIManager.put("ProgressBar.repaintInterval", 14);
        FlatLightLaf.install();
        IntelliJTheme.install( MainUI.class.getClassLoader()
                .getResourceAsStream("themes/Atom One Light Contrast.theme.json" ) );
        JFrame f = new JFrame("ENVARS Assistant");
        Dimension d = new Dimension(800,600);
        f.setPreferredSize(d);
        f.setMinimumSize(d);
        f.setContentPane(new MainUI().mainPane);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        ImageIcon img = new ImageIcon(Objects.requireNonNull(MainUI.class
                .getClassLoader().getResource("icons/app_icon64.png")));
        f.setIconImage(img.getImage());
        f.pack();
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }
}
