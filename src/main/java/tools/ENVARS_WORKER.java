package tools;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import java.io.IOException;
import java.util.Map;

public class ENVARS_WORKER
{
    public final static OS OPERATING_SYSTEM;
    private final static Gson gson;

    static
    {
        gson = new Gson();
	    String os = System.getProperty("os.name");
        if (os.toLowerCase().contains("windows"))
            OPERATING_SYSTEM = OS.WINDOWS;
        else if (os.toLowerCase().contains("linux"))
            OPERATING_SYSTEM = OS.GNU_LINUX;
        else
            OPERATING_SYSTEM = OS.OTHER;
    }

    public static Map<String, String> getEnvVars()
    {
        return System.getenv();
    }

    public static JsonObject getJsonFromString(String strJson)
    {
        try
        {
            return gson.fromJson(strJson, JsonObject.class).getAsJsonObject();
        } catch (JsonSyntaxException e)
        {
            System.err.println("Wrong json format in " + strJson + ". " + e.getMessage());
            return null;
        }
    }

    public static boolean setEnvVar(String varName, String value)
    {
        System.out.printf("Setting %s = %s\n", varName, value);
        switch (OPERATING_SYSTEM)
        {
            case WINDOWS:
            {
                try
                {
                    ProcessBuilder pb = new ProcessBuilder("powershell",
                            "Start-Process",
                            "-Verb",
                            "runAs",
                            String.format("cmd.exe -ArgumentList '/c setx /M %s %s '", varName, value));
                    Process p = pb.start();
                    return p.waitFor() == 0;
                } catch (IOException | InterruptedException e)
                {
                    e.printStackTrace(System.err);
                    return false;
                }

            }
            default:
            {
                System.err.printf("Can't set env vars for OS %s\n", OPERATING_SYSTEM.toString());
            }
        }

        return false;
    }

    public static boolean dropEnVar(String varName)
    {
        System.out.printf("Dropping %s\n", varName);
        switch (OPERATING_SYSTEM)
        {
            case WINDOWS:
            {
                String partCmd = String.format("cmd.exe -ArgumentList '/c SET %1$s= && REG delete HKCU\\Environment /F /V " +
                                "%1$s || REG delete " +
                                "\"HKLM\\SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment\" " +
                                "/F /V %1$s && SETX DUMMY \"\" && REG delete HKCU\\Environment /F /V DUMMY'",
                        varName);
                System.out.println(partCmd);
                try
                {
                    ProcessBuilder pb = new ProcessBuilder("powershell",
                            "Start-Process", "-Verb", "runAs", partCmd);
                    Process p = pb.start();
                    return p.waitFor() == 0;
                } catch (IOException | InterruptedException e)
                {
                    e.printStackTrace(System.err);
                    return false;
                }
            }
            default:
            {
                System.err.printf("Can't drop env vars for OS %s\n", OPERATING_SYSTEM.toString());
            }
        }
        return false;
    }

    public static void main(String[] args)
    {
        System.out.println(OPERATING_SYSTEM.toString());
    }

    public enum OS
    {WINDOWS, GNU_LINUX, OTHER}
}
